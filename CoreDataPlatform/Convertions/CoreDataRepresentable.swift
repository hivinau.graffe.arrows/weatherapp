//
//  CoreDataRepresentable.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import CoreData
import RxSwift
import QueryKit

protocol CoreDataRepresentable {
    
    associatedtype CoreDataType: Persistable
    
    var id: Int64 { get }
    
    func update(entity: CoreDataType)
}

