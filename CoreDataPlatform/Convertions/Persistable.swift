//
//  Persistable.swift
//  CoreDataPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import CoreData
import RxSwift
import QueryKit

protocol Persistable: NSFetchRequestResult, DomainConvertibleType {
    
    static var entityName: String { get }
    static func fetchRequest() -> NSFetchRequest<Self>
}

extension Persistable {
    
    static var primaryAttribute: Attribute<Int64> {
        return Attribute("id")
    }
}
