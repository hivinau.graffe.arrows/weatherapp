//
//  CoreDataPlatform.h
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataPlatform.
FOUNDATION_EXPORT double CoreDataPlatformVersionNumber;

//! Project version string for CoreDataPlatform.
FOUNDATION_EXPORT const unsigned char CoreDataPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataPlatform/PublicHeader.h>


