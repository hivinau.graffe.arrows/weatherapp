//
//  CDCity+CoreDataProperties.swift
//  
//
//  Created by developpeur on 07/08/2019.
//
//

import Foundation
import CoreData


extension CDCity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDCity> {
        return NSFetchRequest<CDCity>(entityName: "CDCity")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var main: CDMain?

}
