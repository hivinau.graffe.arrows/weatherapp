//
//  CDCity+ext.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import CoreData
import Domain
import QueryKit
import RxSwift

extension CDCity {
    static var id: Attribute<Int64> { return Attribute("id") }
    static var name: Attribute<String> { return Attribute("name") }
    static var main: Attribute<Main> { return Attribute("main") }
}

extension CDCity: DomainConvertibleType {
    
    public func asDomain() -> City {
        return City(id: id,
                    name: name!,
                    main: main!.asDomain())
    }
}

extension CDCity: Persistable {
    
    public static var entityName: String {
        return "CDCity"
    }
}

extension City: CoreDataRepresentable {
    
    public typealias CoreDataType = CDCity
    
    public func update(entity: CDCity) {
        
        entity.id = id
        entity.name = name
    }
}


