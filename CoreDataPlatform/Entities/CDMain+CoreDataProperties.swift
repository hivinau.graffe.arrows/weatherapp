//
//  CDMain+CoreDataProperties.swift
//  
//
//  Created by developpeur on 07/08/2019.
//
//

import Foundation
import CoreData


extension CDMain {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDMain> {
        return NSFetchRequest<CDMain>(entityName: "CDMain")
    }

    @NSManaged public var temperature: Double
    @NSManaged public var humidity: Double
    @NSManaged public var temperatureMax: Double
    @NSManaged public var temperatureMin: Double
    @NSManaged public var pressure: Double
    @NSManaged public var city: CDCity?

}
