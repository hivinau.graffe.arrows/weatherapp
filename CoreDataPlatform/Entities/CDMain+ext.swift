//
//  CDMain.swift
//  CoreDataPlatform
//
//  Created by developpeur on 07/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import CoreData
import Domain
import QueryKit
import RxSwift

extension CDMain {
    static var temperature: Attribute<Double> { return Attribute("temperature") }
    static var pressure: Attribute<Double> { return Attribute("pressure") }
    static var temperatureMin: Attribute<Double> { return Attribute("temperatureMin") }
    static var temperatureMax: Attribute<Double> { return Attribute("temperatureMax") }
    static var humidity: Attribute<Double> { return Attribute("humidity") }
}

extension CDMain: DomainConvertibleType {
    
    public func asDomain() -> Main {
        return Main(temperature: temperature,
                    pressure: pressure,
                    humidity: humidity,
                    temperatureMin: temperatureMin,
                    temperatureMax: temperatureMax)
    }
}

extension CDMain: Persistable {
    
    public static var entityName: String {
        return "CDMain"
    }
}

extension CDMain: CoreDataRepresentable {
    
    var id: Int64 {
        return 0
    }
    
    public typealias CoreDataType = CDMain
    
    public func update(entity: CDMain) {
        
        entity.temperature = temperature
        entity.pressure = pressure
        entity.humidity = humidity
        entity.temperatureMin = temperatureMin
        entity.temperatureMax = temperatureMax
    }
}

