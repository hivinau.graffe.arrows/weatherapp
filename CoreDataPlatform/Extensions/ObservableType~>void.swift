//
//  ObservableType~>void.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

public extension ObservableType {
    
    func mapToVoid() -> Observable<Void> {
        
        return map { _ in }
    }
}
