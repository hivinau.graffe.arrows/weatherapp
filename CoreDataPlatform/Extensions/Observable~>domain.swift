//
//  Observable~>domain.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

public extension Observable
where Element: Sequence, Element.Iterator.Element: DomainConvertibleType {
    
    typealias DomainType = Element.Iterator.Element.DomainType
    
    func mapToDomain() -> Observable<[DomainType]> {
        
        return map { sequence -> [DomainType] in
            return sequence.mapToDomain()
        }
    }
}
