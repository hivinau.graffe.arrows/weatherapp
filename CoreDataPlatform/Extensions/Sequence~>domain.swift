//
//  Sequence~>domain.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

public extension Sequence
where Iterator.Element: DomainConvertibleType {
    
    typealias Element = Iterator.Element
    
    func mapToDomain() -> [Element.DomainType] {
        
        return map {
            return $0.asDomain()
        }
    }
}
