//
//  Repository.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import CoreData
import RxSwift
import QueryKit

class Repository<T: CoreDataRepresentable>: IRepository
where T == T.CoreDataType.DomainType {
    
    private let context: NSManagedObjectContext
    private let scheduler: ContextScheduler
    
    init(context: NSManagedObjectContext) {
        self.context = context
        self.scheduler = ContextScheduler(context: context)
    }
    
    public func query(with predicate: NSPredicate? = nil,
               sortDescriptors: [NSSortDescriptor]? = nil) -> Observable<[T]> {
        
        let request = T.CoreDataType.fetchRequest()
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        
        return context.rx.entities(fetchRequest: request)
            .mapToDomain()
            .subscribeOn(scheduler)
    }
    
    public func save(entity: T) -> Observable<Void> {
        
        return entity.sync(in: context)
            .mapToVoid()
            .flatMapLatest(context.rx.save)
            .subscribeOn(scheduler)
    }
    
    public func delete(entity: T) -> Observable<Void> {
        
        return entity.sync(in: context)
            .map { $0 as? NSManagedObject }
            .filter { $0 != nil }
            .map { $0! }
            .flatMapLatest(context.rx.delete)
    }

}
