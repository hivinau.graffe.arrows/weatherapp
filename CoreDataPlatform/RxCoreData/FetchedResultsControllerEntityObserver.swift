//
//  FetchedResultsControllerEntityObserver.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

public final class FetchedResultsControllerEntityObserver<T: NSFetchRequestResult>: NSObject, NSFetchedResultsControllerDelegate {
    
    public typealias Observer = AnyObserver<[T]>
    
    private let observer: Observer
    private let disposeBag = DisposeBag()
    private let controller: NSFetchedResultsController<T>
    
    public init(observer: Observer,
                fetchRequest: NSFetchRequest<T>,
                managedObjectContext context: NSManagedObjectContext,
                sectionNameKeyPath: String?,
                cacheName: String?) {
        
        self.observer = observer
        self.controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                     managedObjectContext: context,
                                                     sectionNameKeyPath: sectionNameKeyPath,
                                                     cacheName: cacheName)
        super.init()
        
        context.perform { [weak self] in
            self?.controller.delegate = self
            
            do {
                
                try self?.controller.performFetch()
                
            } catch let error {
                
                observer.on(.error(error))
            }
            
            self?.sendNextElement()
        }
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        sendNextElement()
    }
    
    public func dispose() {
        
        controller.delegate = nil
    }
    
    private func sendNextElement() {
        self.controller.managedObjectContext.perform { [weak self] in
            
            let entities = self?.controller.fetchedObjects ?? []
            self?.observer.on(.next(entities))
        }
    }
}

