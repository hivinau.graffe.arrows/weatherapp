//
//  CitiesUseCase.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class CitiesUseCase<Repository>: Domain.CitiesUseCase
where Repository: IRepository, Repository.T == City {
    
    private let repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
    }
    
    public func cities() -> Observable<[City]> {
        return repository.query(with: nil, sortDescriptors: [City.CoreDataType.id.descending()])
    }
    
    public func save(city: City) -> Observable<Void> {
        return repository.save(entity: city)
    }
    
    public func delete(city: City) -> Observable<Void> {
        return repository.delete(entity: city)
    }
}


