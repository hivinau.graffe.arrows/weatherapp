//
//  UseCaseProvider.swift
//  CoreDataPlatform
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain

public final class UseCaseProvider: Domain.UseCaseProvider {
    
    private let coreDataStack = CoreDataStack()
    private let cityRepository: Repository<City>
    
    public init() {
        
        cityRepository = Repository<City>(context: coreDataStack.context)
    }
    
    public func makeCitiesUseCase() -> Domain.CitiesUseCase {
        
        return CitiesUseCase(repository: cityRepository)
    }
}
