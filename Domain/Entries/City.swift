//
//  City.swift
//  Domain
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

public struct City: Codable {
    
    public let id: Int64
    public let name: String
    public let main: Main
    
    public init(id: Int64,
                name: String,
                main: Main) {
        
        self.id = id
        self.name = name
        self.main = main
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int64.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        main = try container.decode(Main.self, forKey: .main)
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case main
    }
}


extension City: Equatable {
    
    public static func == (lhs: City, rhs: City) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.main == rhs.main
    }
}

