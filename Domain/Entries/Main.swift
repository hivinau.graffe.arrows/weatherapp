//
//  Main.swift
//  Domain
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

public struct Main: Codable {
    
    public let temperature: Double
    public let pressure: Double
    public let humidity: Double
    public let temperatureMin: Double
    public let temperatureMax: Double
    
    public init(temperature: Double,
                pressure: Double,
                humidity: Double,
                temperatureMin: Double,
                temperatureMax: Double) {
        
        self.temperature = temperature
        self.pressure = pressure
        self.humidity = humidity
        self.temperatureMin = temperatureMin
        self.temperatureMax = temperatureMax
    }
    
    public init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        temperature = try container.decode(Double.self, forKey: .temperature)
        pressure = try container.decode(Double.self, forKey: .pressure)
        humidity = try container.decode(Double.self, forKey: .humidity)
        temperatureMin = try container.decode(Double.self, forKey: .temperatureMin)
        temperatureMax = try container.decode(Double.self, forKey: .temperatureMax)
    }
    
    private enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case pressure
        case humidity
        case temperatureMin = "temp_min"
        case temperatureMax = "temp_max"
    }
}


extension Main: Equatable {
    
    public static func == (lhs: Main, rhs: Main) -> Bool {
        return lhs.temperature == rhs.temperature &&
            lhs.pressure == rhs.pressure &&
            lhs.humidity == rhs.humidity &&
            lhs.temperatureMin == rhs.temperatureMin &&
            lhs.temperatureMax == rhs.temperatureMax
    }
}
