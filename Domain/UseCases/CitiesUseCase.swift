//
//  CitiesUseCase.swift
//  Domain
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

public protocol CitiesUseCase {
    func cities() -> Observable<[City]>
    func save(city: City) -> Observable<Void>
    func delete(city: City) -> Observable<Void>
}
