//
//  UseCaseProvider.swift
//  Domain
//
//  Created by developpeur on 05/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

public protocol UseCaseProvider {
    
    func makeCitiesUseCase() -> CitiesUseCase
}
