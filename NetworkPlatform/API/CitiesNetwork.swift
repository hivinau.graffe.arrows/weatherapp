//
//  CitiesNetwork.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain
import RxSwift

public final class CitiesNetwork {
    
    private let network: Network<City>
    private let apiKey: String
    private let cityName: String
    
    init(network: Network<City>,
         apiKey: String,
         cityName: String) {
        self.network = network
        self.apiKey = apiKey
        self.cityName = cityName
    }
    
    public func fetchCurrentWeather() -> Observable<City> {
        
        let route = "weather?q=\(cityName)&appid=\(apiKey)"
        return network.getItem(route)
    }

}
