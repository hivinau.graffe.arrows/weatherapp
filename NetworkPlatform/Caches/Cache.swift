//
//  Cache.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

final class Cache<T: Encodable>: ICache where T == T.Encoder.DomainType {
    
    enum Error: Swift.Error {
        case saveObject(T)
        case saveObjects([T])
        case fetchObject(T.Type)
        case fetchObjects(T.Type)
    }
    enum FileNames {
        static var objectFileName: String {
            return "\(T.self).dat"
        }
        static var objectsFileName: String {
            return "\(T.self)s.dat"
        }
    }
    
    private let path: String
    private let cacheScheduler = SerialDispatchQueueScheduler(internalSerialQueueName: "hivinau.apps.ios.NetworkPlatform.Caches.queue")
    
    init(path: String) {
        self.path = path
    }
    
    func save(object: T) -> Completable {
        return Completable.create { (observer) -> Disposable in
            guard let url = FileManager.default
                .urls(for: .documentDirectory, in: .userDomainMask).first else {
                    observer(.completed)
                    return Disposables.create()
            }
            
            let fileURL = url.appendingPathComponent(self.path)
                .appendingPathComponent("\(object.id)")
                .appendingPathComponent(FileNames.objectFileName)
            
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: object.encoder,
                                                            requiringSecureCoding: false)
                try data.write(to: fileURL)
                
                observer(.completed)
            } catch {
                observer(.error(Error.saveObject(object)))
            }
            
            return Disposables.create()
            }.subscribeOn(cacheScheduler)
    }
    
    func save(objects: [T]) -> Completable {
        return Completable.create { (observer) -> Disposable in
            guard let directoryURL = self.directoryURL() else {
                observer(.completed)
                return Disposables.create()
            }
            let fileURL = directoryURL
                .appendingPathComponent(FileNames.objectsFileName)
            self.createDirectoryIfNeeded(at: directoryURL)
            
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: objects.map{ $0.encoder },
                                                            requiringSecureCoding: false)
                try data.write(to: fileURL)
                
                observer(.completed)
            } catch {
                observer(.error(error))
            }
            
            return Disposables.create()
            }.subscribeOn(cacheScheduler)
    }
    
    func fetch(withID id: Int64) -> Maybe<T> {
        return Maybe<T>.create { (observer) -> Disposable in
            guard let url = FileManager.default
                .urls(for: .documentDirectory, in: .userDomainMask).first else {
                    observer(.completed)
                    return Disposables.create()
            }
            let fileURL = url.appendingPathComponent(self.path)
                .appendingPathComponent("\(id)")
                .appendingPathComponent(FileNames.objectFileName)
            
            guard let data = try? Data(contentsOf: fileURL),
                let object = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? T.Encoder else {
                observer(.completed)
                return Disposables.create()
            }
            
            observer(MaybeEvent<T>.success(object.asDomain()))
            return Disposables.create()
            }.subscribeOn(cacheScheduler)
    }
    
    func fetchObjects() -> Maybe<[T]> {
        return Maybe<[T]>.create { (observer) -> Disposable in
            guard let directoryURL = self.directoryURL() else {
                observer(.completed)
                return Disposables.create()
            }
            
            let fileURL = directoryURL
                .appendingPathComponent(FileNames.objectsFileName)
            
            guard let data = try? Data(contentsOf: fileURL),
                let objects = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [T.Encoder] else {
                    observer(.completed)
                    return Disposables.create()
            }
            
            observer(MaybeEvent.success(objects.map { $0.asDomain() }))
            return Disposables.create()
            }.subscribeOn(cacheScheduler)
    }
    
    private func directoryURL() -> URL? {
        return FileManager.default
            .urls(for: .documentDirectory,
                  in: .userDomainMask)
            .first?
            .appendingPathComponent(path)
    }
    
    private func createDirectoryIfNeeded(at url: URL) {
        do {
            try FileManager.default.createDirectory(at: url,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
        } catch {
            print("Cache Error createDirectoryIfNeeded \(error)")
        }
    }
}

