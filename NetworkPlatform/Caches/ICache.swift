//
//  ICache.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

protocol ICache {
    
    associatedtype T
    
    func save(object: T) -> Completable
    func save(objects: [T]) -> Completable
    func fetch(withID id: Int64) -> Maybe<T>
    func fetchObjects() -> Maybe<[T]>
}
