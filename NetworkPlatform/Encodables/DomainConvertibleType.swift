//
//  DomainConvertibleType.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

protocol DomainConvertibleType {
    
    associatedtype DomainType: Identifiable
    
    init(with domain: DomainType)
    
    func asDomain() -> DomainType
}

typealias DomainConvertibleCoding = DomainConvertibleType
