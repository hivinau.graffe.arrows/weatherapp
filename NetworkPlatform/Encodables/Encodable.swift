//
//  Encodable.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

protocol Encodable {
    
    associatedtype Encoder: DomainConvertibleCoding
    
    var encoder: Encoder { get }
}
