//
//  Identifiable.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

public protocol Identifiable {
    var id: Int64 { get }
}
