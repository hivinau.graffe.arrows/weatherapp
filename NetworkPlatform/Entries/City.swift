//
//  City.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain

extension City: Identifiable {}

extension City: Encodable {
    
    var encoder: NETCity {
        
        return NETCity(with: self)
    }
}

final class NETCity: NSObject, NSCoding, DomainConvertibleType {
    
    struct Keys {
        static let id = "id"
        static let name = "name"
        static let main = "main"
    }
    
    let id: Int64
    let name: String
    let main: Main
    
    init(with domain: City) {
        self.id = domain.id
        self.name = domain.name
        self.main = domain.main
    }
    
    init?(coder aDecoder: NSCoder) {
        
        guard
            let id = aDecoder.decodeObject(forKey: Keys.id) as? Int64,
            let name = aDecoder.decodeObject(forKey: Keys.name) as? String,
            let main = aDecoder.decodeObject(forKey: Keys.main) as? Main
            else {
                return nil
        }
        self.id = id
        self.name = name
        self.main = main
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: Keys.id)
        aCoder.encode(name, forKey: Keys.name)
        
        /* problème d'encodage */
        //aCoder.encode(main, forKey: Keys.main)
    }
    
    func asDomain() -> City {
        return City(id: id,
                    name: name,
                    main: main)
    }
}


