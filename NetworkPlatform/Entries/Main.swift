//
//  Main.swift
//  NetworkPlatform
//
//  Created by developpeur on 07/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain

extension Main: Identifiable {
    public var id: Int64 {
        return 0
    }
}

extension Main: Encodable {
    
    var encoder: NETMain {
        
        return NETMain(with: self)
    }
}

final class NETMain: NSObject, NSCoding, DomainConvertibleType {
    
    struct Keys {
        static let temperature = "temp"
        static let pressure = "pressure"
        static let humidity = "humidity"
        static let temperatureMin = "temp_min"
        static let temperatureMax = "temp_max"
    }
    
    let temperature: Double
    let pressure: Double
    let humidity: Double
    let temperatureMin: Double
    let temperatureMax: Double
    
    init(with domain: Main) {
        self.temperature = domain.temperature
        self.pressure = domain.pressure
        self.humidity = domain.humidity
        self.temperatureMin = domain.temperatureMin
        self.temperatureMax = domain.temperatureMax
    }
    
    init?(coder aDecoder: NSCoder) {
        guard
            let temperature = aDecoder.decodeObject(forKey: Keys.temperature) as? Double,
            let pressure = aDecoder.decodeObject(forKey: Keys.pressure) as? Double,
            let humidity = aDecoder.decodeObject(forKey: Keys.humidity) as? Double,
            let temperatureMin = aDecoder.decodeObject(forKey: Keys.temperatureMin) as? Double,
            let temperatureMax = aDecoder.decodeObject(forKey: Keys.temperatureMax) as? Double
            else {
                return nil
        }
        self.temperature = temperature
        self.pressure = pressure
        self.humidity = humidity
        self.temperatureMin = temperatureMin
        self.temperatureMax = temperatureMax
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(temperature, forKey: Keys.temperature)
        aCoder.encode(pressure, forKey: Keys.pressure)
        aCoder.encode(humidity, forKey: Keys.humidity)
        aCoder.encode(temperatureMin, forKey: Keys.temperatureMin)
        aCoder.encode(temperatureMax, forKey: Keys.temperatureMax)
    }
    
    func asDomain() -> Main {
        return Main(temperature: temperature,
                    pressure: pressure,
                    humidity: humidity,
                    temperatureMin: temperatureMin,
                    temperatureMax: temperatureMax)
    }
}
