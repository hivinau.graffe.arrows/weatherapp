//
//  Network.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Alamofire
import Domain
import RxAlamofire
import RxSwift

public final class Network<T: Decodable> {
    
    private let endPoint: String
    private let scheduler: ConcurrentDispatchQueueScheduler
    private let decoder: JSONDecoder
    
    public init(_ endPoint: String) {
        
        self.endPoint = endPoint
        self.scheduler = ConcurrentDispatchQueueScheduler(qos: DispatchQoS(qosClass: DispatchQoS.QoSClass.background, relativePriority: 1))
        self.decoder = JSONDecoder()
    }
    
    public func getItem(_ route: String) -> Observable<T> {
        let absolutePath = "\(endPoint)/\(route)"
        
        return RxAlamofire
            .data(.get, absolutePath)
            .debug()
            .observeOn(scheduler)
            .compactMap { [weak self] data -> T? in
                
                return try self?.decoder.decode(T.self, from: data)
            }
    }
}

