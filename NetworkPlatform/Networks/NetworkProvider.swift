//
//  NetworkProvider.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain

final class NetworkProvider {
    
    private let apiEndpoint: String
    private let apiKey: String
    private let cityName: String
    
    public init(apiEndpoint: String,
                apiKey: String,
                cityName: String) {
        
        self.apiEndpoint = apiEndpoint
        self.apiKey = apiKey
        self.cityName = cityName
    }
    
    public func makeCitiesNetwork() -> CitiesNetwork {
        
        let network = Network<City>(apiEndpoint)
        return CitiesNetwork(network: network,
                             apiKey: apiKey,
                             cityName: cityName)
    }
}
