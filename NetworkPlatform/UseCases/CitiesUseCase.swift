//
//  CitiesUseCase.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class CitiesUseCase<Cache>: Domain.CitiesUseCase where Cache: ICache, Cache.T == City {
    
    private let network: CitiesNetwork
    private let cache: Cache
    
    init(network: CitiesNetwork, cache: Cache) {
        self.network = network
        self.cache = cache
    }
    
    func cities() -> Observable<[City]> {
        
        let localCities = cache.fetchObjects().asObservable()
        let remoteCities = network.fetchCurrentWeather()
            .flatMap {
                
                return self.cache.save(objects: [$0])
                    .asObservable()
                    .map(to: [City].self)
                    .concat(Observable.just([$0]))
        }
        
        return localCities.concat(remoteCities)
    }
    
    func save(city: City) -> Observable<Void> {
        
        return Observable.of(())
    }
    
    func delete(city: City) -> Observable<Void> {
        
        return Observable.of(())
    }
    
}
