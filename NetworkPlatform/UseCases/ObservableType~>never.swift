//
//  ObservableType~>never.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

extension ObservableType where Element == Never {
    
    func map<T>(to: T.Type) -> Observable<T> {
        
        return self.flatMap { _ in
            
            return Observable<T>.error(MapFromNever())
        }
    }
}
