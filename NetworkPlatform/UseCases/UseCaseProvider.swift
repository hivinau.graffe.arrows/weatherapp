//
//  UseCaseProvider.swift
//  NetworkPlatform
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain

public final class UseCaseProvider: Domain.UseCaseProvider {
    
    private let networkProvider: NetworkProvider
    
    public init(apiEndpoint: String,
                apiKey: String,
                cityName: String) {
        
        networkProvider = NetworkProvider(apiEndpoint: apiEndpoint,
                                          apiKey: apiKey,
                                          cityName: cityName)
    }
    
    public func makeCitiesUseCase() -> Domain.CitiesUseCase {
        return CitiesUseCase(network: networkProvider.makeCitiesNetwork(),
                             cache: Cache<City>(path: "cities"))
    }
}

