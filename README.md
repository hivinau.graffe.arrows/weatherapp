#  Weather App

## Installation

### Pré-requis

- xcode: version 10.3
- swift: version 5
- cocoapods: version 1.7.5

### Chargement de l'application

Installer les dépendances à l'aide de la commande suivante:

```sh
pod update
```

## À propos

### Documentation

L'application est structurée en 4 modules :
- Domain: définit ce que l'app peut faire (ne dépend d'aucun module ou de UIKit)
- NetworkPlatform:  implémentation du module Domain pour les webservices
- CoreDataPlatform:  implémentation du module Domain pour la persistence de données avec CoreData
- WeatherApp: délivre l'information à l'utilisateur selon le pattern MVVM.

Ce modèle permet de mettre en avant un système modulaire permettant de faciliter une maintenance dans le temps sans que les modules soient impactés entre eux.

### License

Ce projet est maintenu sous la license [MIT](LICENSE).
