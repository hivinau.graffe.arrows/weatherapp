//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by developpeur on 30/07/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        Application.shared.configureMainInterface(in: window)
        window?.makeKeyAndVisible()
        
        return true
    }


}

