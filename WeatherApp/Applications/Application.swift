//
//  Application.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain
import CoreDataPlatform
import NetworkPlatform

public final class Application: NSObject {
    
    public static let shared = Application()
    
    private let coreDataUseCaseProvider: Domain.UseCaseProvider
    private let networkUseCaseProvider: Domain.UseCaseProvider
    
    private override init() {
        
        self.coreDataUseCaseProvider = CoreDataPlatform.UseCaseProvider()
        self.networkUseCaseProvider = NetworkPlatform.UseCaseProvider(apiEndpoint: "https://api.openweathermap.org/data/2.5/",
                                                                      apiKey: "b09a2b1151be7cf9ee9c434e942a6e43",
                                                                      cityName: "Paris")
        
        super.init()
    }
    
    func configureMainInterface(in window: UIWindow?) {
        
        let navigationController = UINavigationController()
        let citiesNavigator = DefaultCitiesNavigator(navigationController: navigationController,
                                                     useCaseProviderForPersistence: coreDataUseCaseProvider,
                                                     useCaseProviderForNetwork: networkUseCaseProvider)
        
        window?.rootViewController = navigationController
        citiesNavigator.showCurrentWeather()
    }
}

