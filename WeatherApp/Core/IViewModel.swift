//
//  IViewModel.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

public protocol IViewModel {
    
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
