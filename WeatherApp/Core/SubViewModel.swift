//
//  SubViewModel.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation

public class SubViewModel<T: Codable>: NSObject {

    internal let item: T
    public let cellIdentifier: String
    
    public init(_ item: T, cellIdentifier: String) {
        
        self.item = item
        self.cellIdentifier = cellIdentifier
        
        super.init()
    }

}
