//
//  TableViewCell.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import UIKit

public class TableViewCell<T: SubViewModel<E>, E: Codable>: UITableViewCell {
    
    internal var viewModel: T?
    
    public func bindViewModel(_ viewModel: T) {
        
        self.viewModel = viewModel
    }
}
