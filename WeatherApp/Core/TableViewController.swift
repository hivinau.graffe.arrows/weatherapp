//
//  TableViewController.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

public class TableViewController<T: IViewModel>: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    public var viewModel: T?
    
    public init() {
        super.init(nibName: "TableViewController", bundle: .main)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        
        Disposer.shared.dispose()
    }
}
