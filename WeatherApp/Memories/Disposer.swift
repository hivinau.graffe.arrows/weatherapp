//
//  Disposer.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import RxSwift

public final class Disposer: NSObject {
    
    public static let shared = Disposer()
    
    private let compositeDisposable: CompositeDisposable
    
    private override init() {
        
        compositeDisposable = CompositeDisposable()
        super.init()
    }
    
    @discardableResult
    public func addDisposable(_ disposable: Disposable?) -> CompositeDisposable.DisposeKey? {
        
        guard let disposable = disposable else {
            
            return nil
        }
        
        return compositeDisposable.insert(disposable)
    }
    
    public func dispose() {
        
        compositeDisposable.dispose()
    }
}
