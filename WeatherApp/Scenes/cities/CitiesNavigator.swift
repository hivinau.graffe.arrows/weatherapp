//
//  CitiesNavigator.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import UIKit
import Domain

public protocol CitiesNavigator {
    
    func showCurrentWeather()
}

public class DefaultCitiesNavigator: CitiesNavigator {
    
    public let navigationController: UINavigationController
    
    private let useCaseProviderForPersistence: Domain.UseCaseProvider
    private let useCaseProviderForNetwork: Domain.UseCaseProvider
    
    public init(navigationController: UINavigationController,
                useCaseProviderForPersistence: Domain.UseCaseProvider,
                useCaseProviderForNetwork: Domain.UseCaseProvider) {
        
        self.navigationController = navigationController
        self.useCaseProviderForPersistence = useCaseProviderForPersistence
        self.useCaseProviderForNetwork = useCaseProviderForNetwork
    }
    
    public func showCurrentWeather() {
        
        let citiesViewController = CitiesViewController()
        citiesViewController.viewModel = CitiesViewModel(navigator: self,
                                                         useCaseForPersistence: useCaseProviderForPersistence.makeCitiesUseCase(),
                                                         useCaseForNetwork: useCaseProviderForNetwork.makeCitiesUseCase())
        
        navigationController.pushViewController(citiesViewController, animated: true)
    }
}
