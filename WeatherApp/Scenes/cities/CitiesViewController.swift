//
//  CitiesViewController.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

public class CitiesViewController: TableViewController<CitiesViewModel> {
    
    private var indicatorView: UIActivityIndicatorView?
    private var indicatorButton: UIBarButtonItem?
    private var refreshButton: UIBarButtonItem?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        bindViewModel()
    }
    
    private func configureViews() {
        
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        
        let nib = UINib(nibName: HeaderTableViewCell.identifier,
                        bundle: .main)
        tableView.register(nib,
                           forCellReuseIdentifier: HeaderTableViewCell.identifier)
        
        let blue = UIColor(hex: "0A5D6A")
        indicatorView = UIActivityIndicatorView(style: .white)
        indicatorView?.tintColor = blue
        indicatorView?.hidesWhenStopped = true
        indicatorButton = UIBarButtonItem(customView: indicatorView!)
        
        refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh,
                                        target: nil,
                                        action: nil)
        
        refreshButton?.tintColor = blue
        navigationItem.setRightBarButton(refreshButton, animated: true)
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: blue]
    }
    
    private func bindViewModel() {
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let refresh = refreshButton!.rx
            .tap
            .asDriver()
        
        let input = CitiesViewModel.Input(trigger: Driver.merge(viewWillAppear, refresh))
        let output = viewModel?.transform(input: input)
        
        let titleDisposable = output?.title.drive(rx.title)
        Disposer.shared.addDisposable(titleDisposable)
        
        let switchingBarButtonsDisposable = output?.loading
            .asObservable()
            .subscribe(onNext: { [weak self] loading in
                
                self?.navigationItem.setRightBarButton(loading ? self?.indicatorButton : self?.refreshButton, animated: true)
            })
        
        Disposer.shared.addDisposable(switchingBarButtonsDisposable)
        
        let loadingDisposable = output?.loading.drive(indicatorView!.rx.isAnimating)
        Disposer.shared.addDisposable(loadingDisposable)
        
        let tableViewBindingDisposable = output?
            .models
            .drive(tableView.rx.items) { [weak self] tableView, row, viewModel in
            
            let indexPath = IndexPath(row: row, section: 0)
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier, for: indexPath)
            
            self?.bindCitiesSubViewModel(viewModel, on: tableViewCell)
            
            return tableViewCell
        }
        
        Disposer.shared.addDisposable(tableViewBindingDisposable)
    }
    
    private func bindCitiesSubViewModel(_ viewModel: CitiesSubViewModel,
                                        on tableViewCell: UITableViewCell) {
        
        switch viewModel.cellIdentifier {
        case HeaderTableViewCell.identifier:
            (tableViewCell as? TableViewCell)?.bindViewModel(viewModel)
        default:
            break
        }
    }
}

