//
//  CitiesViewModel.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import RxCocoa

public class CitiesViewModel: IViewModel {
    
    public struct Input {
        let trigger: Driver<Void>
    }
    
    public struct Output {
        let title: Driver<String>
        let loading: Driver<Bool>
        let models: Driver<[CitiesSubViewModel]>
    }
    
    private let navigator: CitiesNavigator
    private let useCaseForPersistence: CitiesUseCase
    private let useCaseForNetwork: CitiesUseCase
    private let compositeDisposable = CompositeDisposable()
    
    public init(navigator: CitiesNavigator,
                useCaseForPersistence: CitiesUseCase,
                useCaseForNetwork: CitiesUseCase) {
        
        self.navigator = navigator
        self.useCaseForPersistence = useCaseForPersistence
        self.useCaseForNetwork = useCaseForNetwork
    }
    
    public func transform(input: CitiesViewModel.Input) -> CitiesViewModel.Output {
        
        
        let activityIndicator = ActivityIndicator()
        
        let localCities = useCaseForPersistence.cities()
        let remoteCities = useCaseForNetwork.cities()
            .map { [weak self] cities -> [City] in
                
                _ = cities.compactMap { self?.useCaseForPersistence.save(city: $0) }
                
                return cities
        }
        
        let models = input.trigger.flatMapLatest {
            
            Observable.merge(localCities,
                             remoteCities)
                .trackActivity(activityIndicator)
                .flatMap { cities -> Observable<[CitiesSubViewModel]> in
                    
                    return Observable.create { observer in
                        
                        let models = cities.map { CitiesSubViewModel($0, cellIdentifier: HeaderTableViewCell.identifier) }
                        
                        observer.on(.next(models))
                        observer.on(.completed)
                        
                        return Disposables.create()
                    }
                }
                .asDriverOnErrorJustComplete()
        }
        
        let title = Observable.of(NSLocalizedString("appName", comment: "")).asDriverOnErrorJustComplete()
        let loading = activityIndicator.asDriver()
        
        return Output(title: title,
                      loading: loading,
                      models: models)
    }
    
}

