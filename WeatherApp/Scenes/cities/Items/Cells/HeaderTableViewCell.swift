//
//  HeaderTableViewCell.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import UIKit
import Domain

public final class HeaderTableViewCell: TableViewCell<CitiesSubViewModel, City> {
    
    public static let identifier = "HeaderTableViewCell"
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    
    public override func bindViewModel(_ viewModel: CitiesSubViewModel) {
        super.bindViewModel(viewModel)
        
        cityNameLabel.text = viewModel.cityName
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
}
