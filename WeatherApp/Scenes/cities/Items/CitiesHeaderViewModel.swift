//
//  CitiesSubViewModel.swift
//  WeatherApp
//
//  Created by developpeur on 06/08/2019.
//  Copyright © 2019 Hivinau GRAFFE. All rights reserved.
//

import Foundation
import Domain

public final class CitiesSubViewModel: SubViewModel<City> {
    
    public var cityName: String? {
        
        return item.name
    }
}

